from . import serializers, models
from rest_framework import viewsets, permissions
# from .permissions import *


class StateViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.StateSerializer
    queryset = models.State.objects.all()


class EventViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.EventSerializer
    queryset = models.Event.objects.all()
    filter_fields = ('state', 'ig_handle', 'name' )


class GoingViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.GoingSerializer
    queryset = models.Going.objects.all()
    filter_fields = ('event', 'phone_identifier', 'attend')


class EventImageViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.EventImageSerializer
    queryset = models.EventImage.objects.all()


class AdvertViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.AdvertSerializer
    queryset = models.Advert.objects.all()


