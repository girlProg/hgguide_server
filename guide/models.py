from django.db import models


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, null=True)
    modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True
        ordering = ['-modified']


class State(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)

    def __str__(self):
        return self.name


class Event(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)
    description = models.CharField(default='', null=True, blank=True, max_length=1500)
    address = models.CharField(default='', null=True, blank=True, max_length=1500)
    ig_handle = models.CharField(default='', null=True, blank=True, max_length=500)
    phone = models.CharField(default='', null=True, blank=True, max_length=500)
    state = models.ForeignKey(State, null=True, blank=True,on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=False, null=True)
    url = models.CharField(default='', null=True, blank=True, max_length=500)
    long = models.CharField(default='', null=True, blank=True, max_length=500)
    lat = models.CharField(default='', null=True, blank=True, max_length=500)

    def __str__(self):
        return self.name


class Going(BaseModel):
    event = models.ForeignKey(Event, null=True, blank=True, on_delete=models.CASCADE)
    attend = models.BooleanField(default=None, null=True, blank=True)
    phone_identifier = models.CharField(default='', null=True, blank=True, max_length=500)

    def __str__(self):
        return self.event.name + ' - ' + str(self.attend)


class EventImage(BaseModel):
    event = models.ForeignKey(Event, null=True, blank=True, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(blank=True,upload_to='Images/ProductImages')

    def __str__(self):
        return self.event.name


class Advert(BaseModel):
    url = models.CharField(default='', null=True, blank=True, max_length=500)
    name = models.CharField(default='', null=True, blank=True, max_length=500)
    image = models.ImageField(blank=True,upload_to='Images/ProductImages')
    end_date = models.DateTimeField(auto_now_add=False, null=True)

    def __str__(self):
        return self.name






