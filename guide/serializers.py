from rest_framework import serializers
from . import models
from drf_writable_nested.serializers import WritableNestedModelSerializer


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.State
        exclude = ['created', 'modified']


class EventImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.EventImage
        exclude = ['created', 'modified']


class EventSerializer(serializers.ModelSerializer):
    images = EventImageSerializer(many=True)
    state = StateSerializer()

    class Meta:
        model = models.Event
        exclude = ['created', 'modified']


class GoingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Going
        exclude = ['created', 'modified']


class AdvertSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Advert
        exclude = ['created', 'modified']


