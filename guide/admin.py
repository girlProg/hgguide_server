from django.contrib import admin
from reversion.admin import VersionAdmin
from . import models


class GoingInline(admin.StackedInline):
    model = models.Going
    extra = 1


class EventImageInline(admin.StackedInline):
    model = models.EventImage
    extra = 1


class EventAdmin(VersionAdmin):
    inlines = [EventImageInline, GoingInline]


admin.site.register(models.Event, EventAdmin)
admin.site.register(models.State)
admin.site.register(models.Advert)



