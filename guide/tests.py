from django.test import TestCase
from .models import Event, Going


class EventTestCase(TestCase):
    def setUp(self):
        ev = Event.objects.create(name="lion",)
        Going.objects.create(event=ev, attend=True)
        Going.objects.create(event=ev, attend=False)

    def test_only_one_attendance_per_going_per_event(self):
        """a person or device can only have one attendance per event"""
        self.assertEqual(len(Going.objects.all()), 1)

