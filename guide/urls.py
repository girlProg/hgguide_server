from rest_framework import routers
from . import api_views, views
from django.conf.urls import  url, include
from hgserver import settings
from django.conf.urls.static import static


router = routers.DefaultRouter()
router.register('state', api_views.StateViewSet, 'student',)
router.register('event', api_views.EventViewSet, 'event',)
router.register('going', api_views.GoingViewSet, 'going',)
router.register('eventimage', api_views.EventImageViewSet, 'eventimage',)
router.register('adverts', api_views.AdvertViewSet, 'adverts',)


urlpatterns = [
    url(r'^', include(router.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
